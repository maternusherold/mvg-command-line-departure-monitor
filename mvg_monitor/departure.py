#!usr/bin/env python3

import time
import mvg_api


class Departure:

    def __init__(self, time_init, time_last_updated, station, destination,
                 departure_time, label, color, transport_type, issues=None):
        """Modelling a received departure by munich's public transport api.

        The object shall be used when managing and manipulating departure objects.

        :param time_init: time the object was created
        :param time_last_updated: time the object was updated last
        :param station: station departing
        :param destination: destination of this departure
        :param departure_time: time departing;
                ATTENTION: api returns a padding of 3 following zeros
        :param label: mvg line label; number corresponding to line number
        :param color: background color of the line
        :param transport_type: means of transport
        :param issues: issues attached to the departure; optional
        """
        self.time_init = time_init
        self.time_last_updated = time_last_updated
        self.station = station
        self.destination = destination
        self.departure_time = departure_time
        self.label = label
        self.line_color = color
        self.transport_type = transport_type
        self.related_issues = issues

    # TODO: implement
    def time_left(self):
        """Updates the remaining time for the departure.

        :return: time till departure in seconds
        """
        return int(self.departure_time - time.time())

    def get_interruptions(self):
        """Retrieves and updates the related_issues for the departure. """

        # TODO: get all interruptions
        current_interruptions = mvg_api.get_interruptions()

        # TODO: get interruptions based on product and label
        # use label and product to filter current_interruptions

        # append all interruptions to list
        interruptions = []
        self.related_issues = interruptions

    @staticmethod
    def departure_from_dict(departure_info: dict):
        """Creates a departure object from provided information.

        Provided a dict object with a departure related data as returned by the api.
        This should be the go to method as is automatically retrieves further
        information specified in the constructor.

        :param departure_info: dict structured object holding departure information
        :return: departure object
        """
        if not isinstance(departure_info, dict):
            raise ValueError('Provided departure information is not of type dict')

        needed_attributes = ['departureTime', 'product', 'label', 'station',
                             'destination', 'lineBackgroundColor']

        if not set(needed_attributes).issubset(set(departure_info.keys())):
            err = 'Provided departure information does not contain all ' \
                  'relevant attributes: ' \
                  '\n{} vs. {}'.format(departure_info, needed_attributes)
            raise ValueError(err)

        departure = Departure(time_init=time.time(), time_last_updated=time.time(),
                              station=departure_info['station'],
                              destination=departure_info['destination'],
                              departure_time=departure_info['departureTime'],
                              label=departure_info['label'],
                              color=departure_info['lineBackgroundColor'],
                              transport_type=departure_info['label'].upper())
        departure.get_interruptions()

        return departure
